﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace _2017_disco_lights
{
    public partial class Form1 : Form
    {

        Random r = new Random();    // seed the random numbers
        int numberOfSquares = 10;
        int gridSize = 40;

        List<PictureBox> squares = new List<PictureBox>();

        public Form1()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            // get them changing colors
            changeColors();

            // get them moving
            moveSquares(10);

            // get them changing shapes
            morphSquares(10);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // create the initial square grid
            createSquares();

            // Uncomment the below line for animation!
            timer1.Enabled = true;

        }

        private void createSquares()
        {
            for(int y = 0; y <= numberOfSquares; y++)
            {
                for (int x = 0; x <= numberOfSquares; x++)
                {
                    PictureBox pic = new PictureBox();
                    pic.Size = new Size(gridSize, gridSize);
                    pic.Location = new Point(x * gridSize, y * gridSize);
                    pic.BackColor = Color.FromArgb(getR(0,255), getR(0, 255), getR(0, 255));
                    this.Controls.Add(pic);
                    squares.Add(pic);
                }
            }
        }

        private void changeColors()
        {
            foreach(PictureBox p in squares)
            {
                p.BackColor = Color.FromArgb(getR(0, 255), getR(0, 255), getR(0, 255));
            }
        }

        private void moveSquares(int shakeAmount)
        {
            foreach (PictureBox p in squares)
            {
                p.Left = p.Left + getR(-shakeAmount, shakeAmount);
                p.Top = p.Top + getR(-shakeAmount,shakeAmount);
            }
        }

        private void morphSquares(int morphAmount)
        {
            foreach (PictureBox p in squares)
            {
                p.Height = p.Height + getR(-morphAmount, morphAmount);
                p.Width = p.Width + getR(-morphAmount, morphAmount);
            }
        }

        private int getR(int min, int max)
        {
            return r.Next(min, max);
        }
    }
}
